class LowerThan {
  static add (filter) {
    return {
      [filter.field]: {
        '$lt': filter.value
      }
    } 
  }
}

module.exports = LowerThan
