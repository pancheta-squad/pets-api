class GreaterThan {
  static add (filter) {
    return {
      [filter.field]: {
        '$gt': filter.value
      }
    } 
  }
}

module.exports = GreaterThan