const EqualTo = require('./implementations/EqualTo')
const GreaterThan = require('./implementations/GreaterThan')
const LowerThan = require('./implementations/LowerThan')

class ComparationOperators {
  static add(filter) {
    const resultFilter = ComparationOperators._isValid(filter)
    return ComparationOperators.factory(resultFilter.operator).add(resultFilter)
  }
  static factory(operator) {
    const result = {
      'eq': EqualTo,
      'gt': GreaterThan,
      'lt': LowerThan
    }
    return result[operator]
  }

  static _isValid(filter) {
    const result = { ...filter }
    const isANumber = typeof result.value === 'number'
    const isADate = !isNaN(Date.parse(result.value))
    if (!isANumber && !isADate) throw new Error(`Your filter ${result.field} with value ${result.value} is not a number or date`)
    if (isADate) result.value = new Date(result.value).toISOString()

    return result
  }
}

module.exports = ComparationOperators
