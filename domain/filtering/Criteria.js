const ComparationOperators = require('./ComparationOperators')
class Criteria {
  constructor(filters) {
    this.filters = filters
    this.factory = {
      'eq': ComparationOperators,
      'gt': ComparationOperators,
      'lt': ComparationOperators
    }
    this.query = {}
  }

  execute() {
    const filters = this.filters.split(',')
    const regex = /\[(.*)\]\[(.*)\]\[(.*)\]/gm
    for (const item of filters) {
      const result = regex.exec(item)
      const filter = {
        field: result[1],
        operator: result[2],
        value: result[3]
      }
      this.add(filter)
    }
  }

  add(filter) {
    const result = this.factory[filter.operator].add(filter)
    this.query = { ...this.query, ...result }
  }
}

module.exports = Criteria
