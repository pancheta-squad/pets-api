#!/bin/sh

chatId="$1"
telegramToken="$2"
status="$3"
disableNotification="$4"

poutingFaceEmoji="%F0%9F%98%A1"
smileFaceEmoji="%F0%9F%99%82"

statusMessage="${poutingFaceEmoji}%20${CI_PROJECT_NAME}%20Test%20failed%20(%23${status})!"
if [ "${status}" == "OK" ]
  then 
    statusMessage="${smileFaceEmoji}%20${CI_PROJECT_NAME}%20Test%20passed%20(%23${status})!"
fi 

curl \
  -X POST \
  -H "Content-Type: application/json" \
  -d "{\"chat_id\": \"${chatId}\", \"disable_notification\": \"${disableNotification}\"}" \
  https://api.telegram.org/bot${telegramToken}/sendMessage?text=$statusMessage
