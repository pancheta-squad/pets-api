class NotImplementedError extends Error {
  constructor(message = 'Not implemented') {
    super(message)
    this.isOwn = true
  }
}

module.exports = NotImplementedError