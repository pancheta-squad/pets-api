# VPN SERVER

## Pasos para dejar el servidor preparado

1. Crear un usuario

```bash
#: adduser petsapi
#: usermod -aG sudo petsapi
```

**Sin salir de la terminal del servidor, volver a entrar en el servidor con nuestro nuevo usuario y comprobar que tenemos acceso realizando una actualización del sistema**

2. Crear clave ssh

- En el servidor

```bash
$: sudo mkdir -p /home/petsapi/.ssh
$: sudo chown -R petsapi:petsapi /home/petsapi/.ssh
```

- En nuestra máquina

```bash
$: sudo ssh-keygen -t ed25519
$: sudo ssh-copy-id -i /home/nacho/petsapi/.ssh/id_ed25519 petsapi@IPv4_of_your_VPS
$: sudo ssh -i /home/nacho/petsapi/.ssh/id_ed25519  petsapi@IPv4_of_your_VPS
```

Como veis hemos creado nuestra clave ssh en una carpeta, si no, no sería necesaria la opcion `-i`

3. Deshabilitar root y usar access

- En el servidor

```bash
$: sudo nano /etc/ssh/sshd_config
```

Actualizar el archivo `sshd_config` con la siguiente configuración:


```/etc/ssh/sshd_config
PasswordAuthentication no
PermitRootLogin no
```

Reiniciar el servicio de sshd

```bash
$: sudo systemctl reload sshd
```

4. Añadir seguridad de file2ban

```bash
$: sudo apt-get install fail2ban
$: sudo /etc/init.d/fail2ban restart
```

## Instalar docker

[Docker Debian](https://docs.docker.com/engine/install/debian/)

## Install Doppler

[Doppler CLI](https://docs.doppler.com/docs/install-cli#installation)

## Usar doppler para levantar el API

1. Login en doppler `doppler login`

2. Crear directorio de la app `mkdir petsapi`

3. Ejecutar dentro de la carpeta de la app `doppler setup`

4. Crear archivo de deploy para staging `deploy_stg.sh`

```bash
#!{BASH}

VERSION="${1}"

GREEN='\033[0;32m'
ORANGE='\033[0;33m'
RESETCOLOR='\033[0m'

if [ "${VERSION}" == "" ]
  then
    VERSION="latest"
fi

echo -e "${ORANGE}Levantando mongo:5.0 ...${RESETCOLOR}"
docker run --network host --name pets-mongo -d mongo:5.0

echo -e "${ORANGE}Levantando petsapi ...${RESETCOLOR}"
docker pull registry.gitlab.com/pancheta-squad/pets-api:$VERSION
docker stop pets-api
doppler -c stg run -- docker run --rm --network host --env-file <(doppler -c stg secrets download --no-file --f$

echo -e "${GREEN}Usando la version $VERSION de petsapi${RESETCOLOR}"
```

5. Levantar el API provisionando con las variables de doppler en el entorno `bash deploy_stg.sh`

6. Comprobar que el API esta levantada http:/IPv4_of_your_VPS:3050/v1/healthz

7. Preparar el firewall, mucho cuidado con no perder el acceso ssh

![Firewall on petsapi server](./images/firewall.png)

  1. Establecer firewall
  2. Acceso ssh
  3. Entradas http abiertas
  4. Denegar entradas IPv4 no listadas

## Systemctl

Estamos usando systemctl para tener un daemon corriendo y que nos levante la API en caso de que esta se detenga o deje de funcionar. Nuestro deseo es poder seguír tomando esa cervezita si alguna vez el API se cae.

`sudo nano /etc/systemd/system/pets_api_healtzh.service`

1. Generamos el demonio

```bash
[Unit]
Description=Restart petsapi when healthz fails

[Service]
Type=simple
User=petsapi
ExecStart=/bin/bash /home/petsapi/petsapi/healthz.sh
Restart=on-failure

[Install]
WantedBy=default.target
```

2. Generamos el script de healthz

En este script avisamos en nuestro telegram en caso de haber cualquier fallo

3. Restablecer daemon para que coja el cambio

`sudo systemctl daemon-reload`

4. Commandos del systemctl

`sudo systemctl start pets_api_healtzh.service`

`sudo systemctl stop pets_api_healtzh.service`

`sudo systemctl restart pets_api_healtzh.service`

`sudo systemctl status pets_api_healtzh.service`