# Bot de Telegram

Los pasos a seguir son:

A. Agregar un chat del usuario @BotFather, ejecutar `/newbot` y completar la información que pide (nombre, etc.). Tenemos el comando `/help` para ver una ayuda.

B. Como respuesta tendremos: 
- Nombre interno identificativo. 
- Nombre del bot (habitualmente terminado con '_bot').
- Enlace al canal del bot `t.me/nombre_bot`.
- Token de acceso a la HTTP API.

C. Configurar el chat, debemos recoger el identificador; primero agregamos el bot a un canal de telegram, seguidamente enviamos desde cualquier usuario un mensaje al canal y después desde el terminal recogemos las conversaciones:

~~~
curl https://api.telegram.org/bot${TOKEN}/getUpdates
~~~

En las conversaciones tendremos identificado el ID del chat, podemos identificarlos porque pone: `'{my_chat_member: {chat: {id: -100000, title ...}...}...}'`

D. Lanzar mensajes desde la terminal y comprobar que el bot funciona:

~~~
curl \
  -X POST \
  -H "Content-Type: application/json" \
  -d "{"chat_id": "${chatId}"}" \
  api.telegram.org/bot${telegramToken}/sendMessage?text=${statusMessage}
~~~

## Notificaciones

Para que el bot haga notificaciones en función de los resultados de los trabajos de la CI es necesario añadir las variables de CHAT_ID y TOKEN del bot, que se utilizan en `sendMessageWithTelegram.sh` en `GitLab > Settings > CI/CD > Variables`.

En `.gitlab-ci.yml` es necesario dar permisos de ejecución al archivo `sendMessageWithTelegram.sh` añadiendo `chmod +x ./gitlab-ci/sendMessageWithTelegram.sh` en el apartado `pets-api_test > before_script`.
