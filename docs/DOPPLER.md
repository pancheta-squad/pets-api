# Entorno de seguridad Doppler (vault)

## 1. Levantar mongoDB

~~~
docker run --network host -d mongo:5.0
~~~

## 2. Levantar app

- Doppler (`doppler.com`).

El uso de Bash respecto a Dash nos facilita la integración con esta herramienta, por ello allá tanto a través de gitlab-ci como de otras utilidades utilizaremos el uso de las mejoras que Bash nos ofrece.

Como pueden comprobar en la parte de la documentación `https://docs.doppler.com/docs/accessing-secrets` de Doppler; podemos utilizar una línea de código en Bash, como por ejemplo:

~~~
docker run --network host --rm --env-file <(doppler -c dev_env_local secrets download --no-file --format docker) -d --name pets-api  pets-api npm start
~~~

frente al código necesario con una `sh`:

~~~
DOPPLER_NODE_ENV=$(doppler -c dev_env_local secrets get NODE_ENV --plain)
DOPPLER_RESTAPI_PORT=$(doppler -c dev_env_local secrets get RESTAPI_PORT --plain)
DOPPLER_RESTAPI_VERSION=$(doppler -c dev_env_local secrets get RESTAPI_VERSION --plain)
docker run --rm --network host -e NODE_ENV=$DOPPLER_NODE_ENV -e RESTAPI_PORT=$DOPPLER_RESTAPI_PORT -e RESTAPI_VERSION=$DOPPLER_RESTAPI_VERSION -d --name pets-api  pets-api npm start
~~~

Bash está lo suficientemente extendido como para poderlo adoptar sin riesgo. En el caso de que algún usuario utilice otras shells solo deberían adaptar el script para su caso concreto, o bien instalarse Bash.
