# Dependencias de `npm`

## DevDependencies

- Jest
- Supertest

Para instalar una dependencia de desarrollo debe indicarlo al instalador de npm con el atributo `--save-dev`, ejemplo:

~~~
npm install --save-dev jest
~~~

## AppDependencies

- Express
- Dotenv
- uuid
- winston
- mongodb
- @sentry/node

Para instalar una dependencia de la aplicación debe indicarlo al instalador de npm con el atributo `--save`, ejemplo:

~~~
npm install --save uuid
~~~
