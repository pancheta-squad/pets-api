# Arquitectura Hexagonal

![Arquitectura Hexagonal](./images/Hexagonal.png)

En el proyecto nos hemos decidido por usar una arquitectura hexagonal por varios motivos.

1. Altamente escalable
2. Robusta
3. Muy testeable
4. Gran capacidad al cambio

La arquitectura hexagonal es una arquitectura de capas que va de fuera hacia adentro, siendo la capa más externa la de infrastructura.

Cuando hacemos uso de una arquitectura hexagonal debemos seguir una serie de reglas, siendo la más importante **una capa interna nunca hablará con una externa a ella**, gracias a esto conseguimos que cualquier capa externa podría ser cambiada sin afectar a las que tiene en el interior. Si observamos el dibujo, esto permite que **nuestro dominio o core de nuestro producto, quede protegido en el centro,** pudiendo cambiar todo a su alrededor sin afectarle.

La segunda regla y muy importante también. **Todas nuestras herramientas de infrastructura, deben seguir un patrón de Adapter, o Wrapper, para ser inyectadas desde el exterior con una interfaz propia.** **Gracias a esto, podemos cambiar cualquier pieza de infrastructura, sin que nuestras capas internas se vean afectadas**, aportando una gran adaptabilidad a nuestra aplicación.
Esto permite que nuestra app, sea altamente testeable, pudiendo inyectar en los test los mocks o stubs de infrastructura necesarios.

Estas dos serían las reglas más importantes en las que se basa la arquitectura hexagonal.

Además, hacemos uso de Acciones o casos de uso para tener encapsulados los requerimientos de nuestra app.
**Las acciones orquestan distintos Servicios para conseguir su objetivo, consiguiendo tener todo muy ordenado y reutilizable.**
