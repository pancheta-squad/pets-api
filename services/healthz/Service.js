const Healthz = require('../../domain/healthz/Healthz')
const {
  petsRepository: Repository
} = require('../../infrastructure/Adapters')
class Service {
  static async isHealthzy () {
    const healthz = new Healthz()
    const databaseStatus = await Repository.findOneForHealthz()
    return { 
      status: healthz.getStatus(),
      database: databaseStatus
    }
  }
}

module.exports = Service
