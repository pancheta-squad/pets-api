const NotImplementedError = require("../../utils/NotImplementedError")

class Errors {
  toLog () {
    throw new NotImplementedError()
  }
  toResponse () {
    throw new NotImplementedError()
  }
}
module.exports = Errors