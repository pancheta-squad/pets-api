const Logger = require("./Logger")
const { logger } = require('./WinstonLogger')
const { getRequestContext } = require('../RequestContext')
class LoggerWinstonAdapter extends Logger {
  constructor () {
    super()
    this.logger = logger
  }
  debug(message, objectToLog = {}) {
    this.logger.debug(message, { requestContext: getRequestContext(), ...objectToLog })
  }
  info(message, objectToLog = {}) {
    this.logger.info(message, { requestContext: getRequestContext(), ...objectToLog })
  }
  warn(message, objectToLog = {}) {
    this.logger.warn(message, { requestContext: getRequestContext(), ...objectToLog })
  }
  error(message, objectToLog = {}) {
    if (objectToLog.message === message) {
      this.logger.error({ requestContext: getRequestContext(), ...objectToLog })
    } else {
      this.logger.error(message, { requestContext: getRequestContext(), ...objectToLog })
    }
  }
}

module.exports = LoggerWinstonAdapter