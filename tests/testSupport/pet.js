const request = require('supertest')
const app = require('../../index')

const createPet = async (name = 'Ahcul', date) => {
  const pet = {
    name,
    bornAt: date,
    title: 'Ahcul is great!!!',
    description: 'Ahcul is a very funny dog, very kindle and confident',
    imagesURL: [
      'https://live.staticflickr.com/5088/5323961120_0172112bcb_b.jpg',
      'https://live.staticflickr.com/3433/3927529272_e6e5448807.jpg'
    ]
  }
  return await postAPet(pet)
}

const postAPet = async (pet) => {
  return await request(app)
    .post(`/${process.env.RESTAPI_VERSION}/pets`)
    .send(pet)
}

const preparePetAge = (petAge) => {
  let date = new Date()
  date.setFullYear(date.getFullYear() - petAge)
  date = date.toISOString()

  return date
}

module.exports = {
  createPet,
  postAPet,
  preparePetAge
}