require('../../infrastructure/environment').loadEnvVars()
process.env.NODE_ENV = 'testing'
const request = require('supertest')
const ErrorAdapter = require('../../infrastructure/errors/ErrorAdapter')
const HealthzAction = require('../../actions/healthz/HealthzAction')
const app = require('../../index')
const generateV4UUID = require('../../utils/generateUUID')
const BAD_REQUEST = 400
beforeEach(() => {
  jest.resetAllMocks()
})
describe('ErrorAdapter', () => {
  it('is handling our errors', () => {
    const error = new Error('Test message: Queue already processed')
    const errorAdapter = new ErrorAdapter(error, 'FromRabbitError')
    const errorLogged = errorAdapter.toLog()
    const errorResponsed = errorAdapter.toResponse()
    expect(errorLogged.errorId).toBeTruthy()
    expect(errorLogged.message).toBe(error.message)
    expect(errorLogged.code).toBe('FromRabbitError')
    expect(errorLogged.isOwn).toBe(false)
    expect(errorLogged.data).toMatchObject({})
    expect(errorLogged.type).toBe('Error')
    expect(errorLogged.status).toBe('ko')
    expect(errorLogged.stack).toBeTruthy()
    expect(errorLogged.createdAt).toBeTruthy()
    expect(errorResponsed.id).toBeTruthy()
    expect(errorResponsed.message).toBe(error.message)
    expect(errorResponsed.type).toBe('Error')
    expect(errorResponsed.status).toBe('ko')
    expect(errorResponsed.createdAt).toBeTruthy()
  })
})
describe('Healthz endpoint', () => {
  it('error is well formated with own correlation id', async () => {
    const testMessage = 'Test-message: Your database is off'
    jest.spyOn(HealthzAction, 'invoke').mockImplementation(() => {
      return new Promise((resolve, reject) => {
        const error = new Error(testMessage)
        error.statusCode = 406
        reject(error)
      })
    })
    
    const response = await request(app)
      .get(`/${process.env.RESTAPI_VERSION}/healthz`)
    
    expect(response.statusCode).toBe(BAD_REQUEST)
    expect(response.body.id).toBeTruthy()
    expect(response.header.tracerequestid).toBeTruthy()
    expect(response.body.message).toBe(testMessage)
    expect(response.body.status).toBe('ko')
    expect(response.body.type).toBe('Error')
    expect(response.body.createdAt).toBeTruthy()
  })
  it('error is well formated with other service traceRequestId', async () => {
    const testMessage = 'Test-message: Your database is off'
    jest.spyOn(HealthzAction, 'invoke').mockImplementation(() => {
      return new Promise((resolve, reject) => {
        const error = new Error(testMessage)
        error.statusCode = 406
        reject(error)
      })
    })
    const traceRequestIdFromOtherService = generateV4UUID()
    
    const response = await request(app)
      .get(`/${process.env.RESTAPI_VERSION}/healthz`)
      .set('traceRequestId', traceRequestIdFromOtherService)
    
    expect(response.statusCode).toBe(BAD_REQUEST)
    expect(response.body.id).toBeTruthy()
    expect(response.header.tracerequestid).toBe(traceRequestIdFromOtherService)
    expect(response.header.tracerequestid === response.body.id).toBeFalsy()
    expect(response.body.message).toBe(testMessage)
    expect(response.body.status).toBe('ko')
    expect(response.body.type).toBe('Error')
    expect(response.body.createdAt).toBeTruthy()
  })
})
