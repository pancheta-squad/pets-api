require('../../infrastructure/environment').loadEnvVars()
const NODE_ENV = 'testing'
process.env.NODE_ENV = NODE_ENV
const Connection = require('../../infrastructure/mongoConnection')
const request = require('supertest')
const app = require('../../index')
const SUCCESS = 200
const BAD_REQUEST = 400

describe('The API', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })

  test('throws and error if has not environment variables', async () => {
    process.env.NODE_ENV = ''
    const response = await request(app).get(`/${process.env.RESTAPI_VERSION}/healthz`)
    
    expect(response.statusCode).toBe(BAD_REQUEST)
    expect(response.body.status).toBe('ko')
    
    process.env.NODE_ENV = NODE_ENV
  })
  test('is available', async () => {
    const response = await request(app).get(`/${process.env.RESTAPI_VERSION}/healthz`)
    
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body.status).toBe('ok')
    expect(response.body.environment.NODE_ENV).toBeTruthy()
  })
  test('has database state info', async () => {
    const response = await request(app).get(`/${process.env.RESTAPI_VERSION}/healthz`)
    
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body.status).toBe('ok')
    expect(response.body.database).toBe('ok')
  })
  test('has database state info', async () => {
    jest.spyOn(Connection, 'getDB').mockImplementation(() => {
      return new Promise((resolve, reject) => {
        const error = new Error('Test-message: Your database is off')
        reject(error)
      })
    })

    const response = await request(app).get(`/${process.env.RESTAPI_VERSION}/healthz`)
    
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body.status).toBe('ok')
    expect(response.body.database).toBe('ko')
  })
})
