const { 
  environmentVariablesToAssert,
  assertVariable
} = require('../../infrastructure/environment')
const NODE_ENV = 'testing'
process.env.NODE_ENV = NODE_ENV

describe('Variable', () => {
  test('NODE_ENV is empty in environmentVariablesToAssert', async () => {
    process.env.NODE_ENV = ''
    try {
      environmentVariablesToAssert()
      expect(process.env.NODE_ENV !== '').toBe(true)
    } catch(error) {
      expect(error.message).toBe('Variable NODE_ENV is empty!')
    }
    process.env.NODE_ENV = NODE_ENV
  })
  test('TEST_UNDEFINED_VARIABLE is undefined when assertVariable', async () => {
    try {
      assertVariable(process.env.TEST_UNDEFINED_VARIABLE, 'TEST_UNDEFINED_VARIABLE')
      expect(process.env.TEST_UNDEFINED_VARIABLE !== undefined).toBe(true)
    } catch(error) {
      expect(error.message).toBe('Variable TEST_UNDEFINED_VARIABLE is undefined!')
    }
  })
})
