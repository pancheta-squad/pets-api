const Pets = require('../../services/pets')

class Create {
  static async invoke (pet) {
    return Pets.service.create(pet)
  }
}

module.exports = Create