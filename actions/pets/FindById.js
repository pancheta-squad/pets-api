const Pets = require('../../services/pets')

class FindById {
  static async invoke (id) {
    return Pets.service.findById(id)
  }
}

module.exports = FindById