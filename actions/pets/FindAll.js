const Pets = require('../../services/pets')

class FindAll {
  static async invoke () {
    return Pets.service.findAll()
  }
}

module.exports = FindAll